package com.logicinfo.extra.store;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.logicinfo.extra.util.Constant;
import com.logicinfo.extra.util.InActiveTrackIdUtils;

public class TrackingOrderList {

	private static Logger LOGGER = Logger.getLogger(TrackingOrderList.class);

	Connection simConn;
	ExtraDBConnection extraDBConnection;
	CallableStatement inActivecommOrderCallableStmt;
	PreparedStatement inActivecommOrderPreparedStmt;
	PreparedStatement inActiveShipmentCarrierPreparedStmt;
	PreparedStatement inActiveWmsShipmentCarrierPreparedStmt;
	PreparedStatement inactiveSimShipmentUpdatepreparedStmt;
	PreparedStatement inactiveWmsShipmentUpdatepreparedStmt;

	public TrackingOrderList() throws SQLException, Exception {
		extraDBConnection = new ExtraDBConnection();
		extraDBConnection.loadProperties(Constant.SIM);

		// callable statement
		inActivecommOrderCallableStmt = extraDBConnection.getSimConnection()
				.prepareCall(InActiveTrackIdUtils.executeprocedure);

		// prepared statement to fetch the records...
		inActivecommOrderPreparedStmt = extraDBConnection.getSimConnection()
				.prepareCall(InActiveTrackIdUtils.fetchrecordsfromsim);

		// prepared statement to fetch records based on ID's
		inActiveShipmentCarrierPreparedStmt = extraDBConnection.getSimConnection()
				.prepareCall(InActiveTrackIdUtils.fetchrecordsbasedonid);

		// sim update tables
		inactiveSimShipmentUpdatepreparedStmt = extraDBConnection.getSimConnection()
				.prepareStatement(InActiveTrackIdUtils.updatesimheadertable);

		// WMS connection fetch all inactive tracking id records
		extraDBConnection.loadProperties(Constant.WMS);
		inActiveWmsShipmentCarrierPreparedStmt = extraDBConnection.getSimConnection()
				.prepareCall(InActiveTrackIdUtils.fetchwmsallinactivetrackingrecords);

		// wms update
		inactiveWmsShipmentUpdatepreparedStmt = extraDBConnection.getSimConnection()
				.prepareStatement(InActiveTrackIdUtils.updatewmsheadertable);

	}

	/* Get Sim all records */

	public List<InActiveTrackingOrders> getInactiveOrderListFromSim() throws SQLException {

		List<InActiveTrackingOrders> inActiveTrackingOrdersList = new ArrayList<InActiveTrackingOrders>();
		ResultSet inActiveTrackingOrdersResultSet = null;

		this.inActivecommOrderCallableStmt.registerOutParameter(1, java.sql.Types.NUMERIC);
		this.inActivecommOrderCallableStmt.registerOutParameter(2, java.sql.Types.VARCHAR);
		this.inActivecommOrderCallableStmt.executeUpdate();

		int num = this.inActivecommOrderCallableStmt.getInt(1);

		if (num == 0) {

			inActivecommOrderPreparedStmt.setString(1, "N");
			inActiveTrackingOrdersResultSet = inActivecommOrderPreparedStmt.executeQuery();

		} else {

			System.out.println("no records are available");
			return inActiveTrackingOrdersList;
		}

		InActiveTrackingOrders inActiveTrackingOrdersObj = null;

		while (inActiveTrackingOrdersResultSet.next()) {

			inActiveTrackingOrdersObj = new InActiveTrackingOrders();

			inActiveTrackingOrdersObj.setCustomerOrderNo(inActiveTrackingOrdersResultSet.getString("CUST_ORDER_NO"));
			inActiveTrackingOrdersObj.setTrackingId(inActiveTrackingOrdersResultSet.getString("TRACKING_ID"));
			inActiveTrackingOrdersObj.setCarrierId(inActiveTrackingOrdersResultSet.getInt("SHIP_CARRIER_ID"));
			inActiveTrackingOrdersObj.setProcessInd(inActiveTrackingOrdersResultSet.getString("PROCESS_IND"));
			inActiveTrackingOrdersList.add(inActiveTrackingOrdersObj);
		}
		System.out.println("The Size of the List is " + inActiveTrackingOrdersList.size());
		return inActiveTrackingOrdersList;
	}

	public List<InActiveTrackingOrders> getInactiveOrderListFromWms() throws SQLException {

		List<InActiveTrackingOrders> inActiveTrackingOrdersWmsList = new ArrayList<InActiveTrackingOrders>();

		ResultSet inActiveTrackingOrdersWmsResultSet = null;
		InActiveTrackingOrders inActiveTrackingOrdersWmsObj = null;

		inActiveWmsShipmentCarrierPreparedStmt.setString(1, "N");
		inActiveWmsShipmentCarrierPreparedStmt.setString(2, "Cancel");

		inActiveTrackingOrdersWmsResultSet = inActiveWmsShipmentCarrierPreparedStmt.executeQuery();

		while (inActiveTrackingOrdersWmsResultSet.next()) {
			inActiveTrackingOrdersWmsObj = new InActiveTrackingOrders();

			inActiveTrackingOrdersWmsObj
					.setCustomerOrderNo(inActiveTrackingOrdersWmsResultSet.getString("CUST_ORDER_NBR"));
			inActiveTrackingOrdersWmsObj
					.setTrackingId(inActiveTrackingOrdersWmsResultSet.getString("CARRIER_SHIPMENT_NBR"));
			inActiveTrackingOrdersWmsObj.setCarrierCode(inActiveTrackingOrdersWmsResultSet.getString("CARRIER_CODE"));
			inActiveTrackingOrdersWmsList.add(inActiveTrackingOrdersWmsObj);

		}

		System.out.println("From Wms  List Size is " + inActiveTrackingOrdersWmsList.size());
		return inActiveTrackingOrdersWmsList;

	}

	public Map<Integer, InActiveTrackingOrdersShipmentCarrier> getShipmentCarrierId() throws SQLException {

		ResultSet inActiveTrackingOrdersBasedonidResultSet = null;
		Map<Integer, InActiveTrackingOrdersShipmentCarrier> shipmentCarrierMap = new HashMap<Integer, InActiveTrackingOrdersShipmentCarrier>();
		inActiveTrackingOrdersBasedonidResultSet = inActiveShipmentCarrierPreparedStmt.executeQuery();
		while (inActiveTrackingOrdersBasedonidResultSet.next()) {
			shipmentCarrierMap.put(inActiveTrackingOrdersBasedonidResultSet.getInt("ID"),
					new InActiveTrackingOrdersShipmentCarrier(
							inActiveTrackingOrdersBasedonidResultSet.getString("CODE"),
							inActiveTrackingOrdersBasedonidResultSet.getString("DESCRIPTION")));
		}

		return shipmentCarrierMap;
	}

	public void updateProcessIndFlagAndDate(AwbCancellationRequest awbcancellationRequestObj, String source) {

		try {

			if ("SIM".equals(source)) {
				this.inactiveSimShipmentUpdatepreparedStmt.setString(1, awbcancellationRequestObj.getOrderNo());
				this.inactiveSimShipmentUpdatepreparedStmt.setString(2,
						awbcancellationRequestObj.getCourierTrackingNo());
				this.inactiveSimShipmentUpdatepreparedStmt.executeUpdate();
			} else {

				inactiveWmsShipmentUpdatepreparedStmt.setString(1, awbcancellationRequestObj.getOrderNo());
				inactiveWmsShipmentUpdatepreparedStmt.setString(2, awbcancellationRequestObj.getCourierTrackingNo());
				inactiveWmsShipmentUpdatepreparedStmt.setString(3, awbcancellationRequestObj.getCarrier());
				this.inactiveWmsShipmentUpdatepreparedStmt.executeUpdate();
			}

			System.out.println("Record successfully updated");

		} catch (SQLException e) {
			LOGGER.error("TrackingOrderList::updateProcessIndFlagAndDate method SQLException" + e);
		} catch (Exception e) {
			LOGGER.error("TrackingOrderList::updateProcessIndFlagAndDate method SQLException" + e);
		}

	}

}
