package com.logicinfo.extra.store;

import java.io.Serializable;

public class AwbCancellationRequest implements Serializable {

	private static final long serialVersionUID = 1L;

	private String orderNo;

	private String courierTrackingNo;

	private String carrier;

	public AwbCancellationRequest(String orderNo, String courierTrackingNo, String carrier) {

		this.orderNo = orderNo;
		this.courierTrackingNo = courierTrackingNo;
		this.carrier = carrier;
	}

	public AwbCancellationRequest() {

	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getCourierTrackingNo() {
		return courierTrackingNo;
	}

	public void setCourierTrackingNo(String courierTrackingNo) {
		this.courierTrackingNo = courierTrackingNo;
	}

	public String getCarrier() {
		return carrier;
	}

	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}

}
