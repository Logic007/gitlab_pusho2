package com.logicinfo.extra.store;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.logicinfo.extra.util.Constant;
import com.logicinfo.extra.util.PropertiesReader;

import oracle.jdbc.driver.OracleDriver;

public class ExtraDBConnection {
	
	private static Logger LOGGER = Logger.getLogger(ExtraDBConnection.class);

	public String DBURL;
	public String DBUsername;
	public String DBPassword;
	public String dbType;

	/* This Function loads the properties from batch.properties Property file */
	public void loadProperties(String dbType) {

		if (dbType.equals(Constant.SIM)) {
			this.DBURL = PropertiesReader.getProperty("sim14.db.jdbc-url");
			this.DBUsername = PropertiesReader.getProperty("sim14.db.user");
			this.DBPassword = PropertiesReader.getProperty("sim14.db.password");
		} else if (dbType.equals(Constant.WMS)) {
			this.DBURL = PropertiesReader.getProperty("wms.db.jdbc-url");
			this.DBUsername = PropertiesReader.getProperty("wms.db.user");
			this.DBPassword = PropertiesReader.getProperty("wms.db.password");
		}

	}

	/* This Function creates the JDBC Connection */
	public Connection getSimConnection() throws Exception {
		Connection jdbcConnection = null;
		if (this.DBURL != null && this.DBUsername != null && this.DBUsername != null && !this.DBURL.trim().equals("")
				&& !this.DBUsername.trim().equals("") && !this.DBUsername.trim().equals("")) {

			try {

				/* Creation of JDBC Connection */
				LOGGER.info("Starting connection");
				DriverManager.registerDriver(new OracleDriver());
				jdbcConnection = DriverManager.getConnection(DBURL, DBUsername, DBPassword);
				LOGGER.info(" Connected driver Version:" + jdbcConnection.getMetaData().getDriverVersion());
				/* Initializing the DAOs */

			} catch (SQLException e) {

				LOGGER.error("SQLException at getConnection " + e);
				throw new Exception("Exception occured while opening the db connection: " + e.getMessage());

			}

		} else {
			LOGGER.error("Exception occured while reading the db properties");
			throw new Exception("Exception occured while reading the db properties");

		}

		return jdbcConnection;
	}
	
	
	public static void closeConnection(Connection connection) {
		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException exception) {

				LOGGER.error("Exception while closing the DB Connection: " + exception);

			}
		}
	}
}
