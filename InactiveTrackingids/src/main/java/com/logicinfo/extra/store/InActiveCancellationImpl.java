package com.logicinfo.extra.store;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

public class InActiveCancellationImpl {

	public static void main(String args[]) {
		Logger LOGGER = Logger.getLogger(InActiveCancellationImpl.class);

		TrackingOrderList trackingOrderListObj = null;
		List<InActiveTrackingOrders> inActiveTrackingSimOrdersList = new ArrayList<InActiveTrackingOrders>();
		List<InActiveTrackingOrders> inActiveTrackingWmsOrdersList = new ArrayList<InActiveTrackingOrders>();
		Map<Integer, InActiveTrackingOrdersShipmentCarrier> shipmentCarrierMap = null;

		try {
			trackingOrderListObj = new TrackingOrderList();

		} catch (Exception e) {
			LOGGER.error("InActiveCancellationImpl::Exception in main method" + e);
		}

		if (null != trackingOrderListObj) {

			try {
				inActiveTrackingSimOrdersList = trackingOrderListObj.getInactiveOrderListFromSim();
				inActiveTrackingWmsOrdersList = trackingOrderListObj.getInactiveOrderListFromWms();
			} catch (Exception ex) {
				LOGGER.error("InActiveCancellationImpl::Exception in main method" + ex);
			}
		}

		if (inActiveTrackingSimOrdersList.size() >= 0)
			try {
				shipmentCarrierMap = trackingOrderListObj.getShipmentCarrierId();
			} catch (SQLException et) {
				LOGGER.error("InActiveCancellationImpl::Exception in main method" + et);
			}

		for (InActiveTrackingOrders inactiveSimOrderObj : inActiveTrackingSimOrdersList) {

			LOGGER.info("Customer order no : " + inactiveSimOrderObj.getCustomerOrderNo() + "Tracking id : "
					+ inactiveSimOrderObj.getTrackingId() + "Carrier name : "
					+ shipmentCarrierMap.get(inactiveSimOrderObj.getCarrierId()).getDescription());
			
			System.out.println("Customer order no : " + inactiveSimOrderObj.getCustomerOrderNo() + "Tracking id : "
					+ inactiveSimOrderObj.getTrackingId() + "Carrier name : "
					+ shipmentCarrierMap.get(inactiveSimOrderObj.getCarrierId()).getDescription());

			AwbCancellationRequest awbcancellationRequestObj = new AwbCancellationRequest(
					inactiveSimOrderObj.getCustomerOrderNo(), inactiveSimOrderObj.getTrackingId(),
					shipmentCarrierMap.get(inactiveSimOrderObj.getCarrierId()).getDescription());

			ShipmentCancellationClient.callInactiveOrdrdelete(awbcancellationRequestObj, "SIM");

		}

		for (InActiveTrackingOrders inactiveWmsOrderObj : inActiveTrackingWmsOrdersList) {
			/*LOGGER.info("Customer order no : " + inactiveWmsOrderObj.getCustomerOrderNo() + "Tracking id : "
					+ inactiveWmsOrderObj.getTrackingId() + "Carrier name : "
					+ shipmentCarrierMap.get(inactiveWmsOrderObj.getCarrierId()).getDescription());*/
			
			AwbCancellationRequest awbcancellationRequestObj = new AwbCancellationRequest(
					inactiveWmsOrderObj.getCustomerOrderNo(), inactiveWmsOrderObj.getTrackingId(),
					inactiveWmsOrderObj.getCarrierCode());

			ShipmentCancellationClient.callInactiveOrdrdelete(awbcancellationRequestObj, "WMS");
		}

		

	} // main methods ends ...
}
